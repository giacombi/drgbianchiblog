+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Consultant Cardiac Surgeon"
  company = "Fondazione Toscana 'G. Monasterio'"
  company_url = "www.ftgm.it"
  location = "Italy"
  date_start = "2010-08-01"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Perform Surgery for Acquired Cardiac Disease
  * ECMO
  * Clinical Research
  """

[[experience]]
  title = "PhD Student"
  company = "Scuola Superiore Sant'Anna"
  company_url = "www.sssup.it"
  location = "Pisa, ITALY"
  date_start = "2010-01-20"
  date_end = "2013-12-18"
  description = """PhD in Translational Research"""

[[experience]]
  title = "Research Fellow"
  company = "Artificial Organs Lab - University of Maryland"
  company_url = "www.aol.umaryland.edu"
  location = "10 South Pine Street MSTF 434A, Baltimore, MD 21201"
  date_start = "2009-04-01"
  date_end = "2010-01-15"
  description = """
  Responsibilities include:
  
  * Surgical Models of Heart Failure
  * Mesenchymal Stem Cells Research
  
  """

+++
