+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Cardiac Surgery",
    "Heart Failure",
    "Minimally invasive valve surgery",
    "Data Science"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "ECMO Diploma"
  institution = "Hopital La Petriére Salpetriére - Paris - FRANCE"
  year = 2017

[[education.courses]]
  course = "PhD in Translational Medicine"
  institution = "Scuola Superiore Sant'Anna - Pisa - ITALY"
  year = 2013

[[education.courses]]
  course = "CardioThoracic Specialty Degree"
  institution = "University of Florence - Florence - ITALY"
  year = 2009
 
[[education.courses]]
  course = "Medical Degree (MD)"
  institution = "University of Florence - Florence - ITALY"
  year = 2004

+++

# Biography

Giacomo Bianchi is Staff Cardiac Surgeon at Ospedale del Cuore in Massa.
