+++
title = "Thirty-day in-vivo performance of a wearable artificial pump-lung for ambulatory respiratory support"
date = 2012-01-01
authors = ["Zhongjun J Wu", "Tao Zhang", "Giacomo Bianchi", "Xufeng Wei", "Ho-Sung Son", "Kang Zhou", "Pablo G Sanchez", "Jose Garcia", "Bartley P Griffith"]
publication_types = ["2"]
abstract = "BACKGROUND: The purpose of this study was to evaluate the long-term in-vivo hemodynamics, gas transfer, and biocompatibility of an integrated artificial pump-lung (APL) developed for ambulatory respiratory support.METHODS: The study was conducted in an ovine model by surgically placing the APL between the right atrium and pulmonary artery. Nine sheep were implanted. Heparin was infused as an anticoagulant. The device flow, gas transfer, and plasma free hemoglobin were measured daily. Hematologic data, platelet activation, and blood biochemistry were assessed twice a week. After 30 days, the sheep were euthanized for postmortem examination. The explanted devices were examined for gross thrombosis.RESULTS: Five sheep survived for 29 to 31 days and were electively terminated. Four sheep died or were terminated early owing to mechanical failure of intravenous lines or device. The APL devices in the 5 long-term animals were capable of delivering an oxygen transfer rate of 148$pm$18 mL/min at a flow rate of 2.99$pm$0.46 L/min with blood oxygen saturation of 96.7%$pm$1.3%. The device flow and oxygen transfer were stable over 30 days. The animals had normal end-organ functions except for surgery-related transient alteration in kidney function, liver function, and cell and tissue injury. There was no hemolysis. The device flow path and membrane surface were free of gross thrombus.CONCLUSIONS: The APL exhibited the capability of providing respiratory support with excellent biocompatibility, long-term reliability, and the potential for bridging to lung transplant."
selected = "false"
publication = "*Ann Thorac Surg*"
doi = "10.1016/j.athoracsur.2011.08.076"
+++

