+++
title = "eComment. Autologous pericardium is superior to conventional bovine patch in congenital heart disease reconstructive surgery: an appraisal for tissueengineered xenograft"
date = 2013-10-01
authors = ["Giacomo Bianchi"]
publication_types = ["2"]
abstract = ""
selected = "false"
publication = "*Interact Cardiovasc Thorac Surg*"
doi = "10.1093/icvts/ivt368"
+++

