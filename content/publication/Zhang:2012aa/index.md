+++
title = "A novel wearable pump-lung device: in vitro and acute in vivo study"
date = 2012-01-01
authors = ["Tao Zhang", "Xufeng Wei", "Giacomo Bianchi", "Philip M Wong", "Brian Biancucci", "Bartley P Griffith", "Zhongjun J Wu"]
publication_types = ["2"]
abstract = "BACKGROUND: To provide long-term ambulatory cardiopulmonary and respiratory support for adult patients, a novel wearable artificial pump-lung device has been developed. The design features and in vitro and acute in vivo performance of this device are reported.METHODS: This device features a uniquely designed hollow-fiber membrane bundle integrated with a magnetically levitated impeller that together form one ultracompact pump-lung device, which can be placed like current paracorporeal ventricular assist devices to allow ambulatory support. The device is 117 mm in length and 89 mm in diameter and has a priming volume of 115 ml. In vitro hydrodynamic, gas transfer and biocompatibility experiments were carried out in mock flow-loops using ovine blood. Acute in vivo characterization was conducted in an ovine model by surgically implanting the device between right atrium and pulmonary artery.RESULTS: The in vitro results show that the device with a membrane surface area of 0.8 m(2) was capable of pumping blood from 1 to 4 liters/min against a wide range of pressures and transferring oxygen at a rate of up to 180 ml/min at a blood flow of 3.5 liters/min. Standard hemolysis tests demonstrated low hemolysis at the targeted operating condition. The acute in vivo results also confirmed that the device can provide sufficient oxygen transfer with excellent biocompatibility.CONCLUSIONS: Based on in vitro and acute in vivo study findings, this highly integrated wearable pump-lung device can provide efficient respiratory support with good biocompatibility and it is ready for long-term evaluation."
selected = "false"
publication = "*J Heart Lung Transplant*"
doi = "10.1016/j.healun.2011.08.022"
+++

