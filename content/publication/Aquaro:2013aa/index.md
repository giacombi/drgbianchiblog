+++
title = "3D CMR mapping of metabolism by hyperpolarized 13C-pyruvate in ischemia-reperfusion"
date = 2013-06-01
authors = ["Giovanni Donato Aquaro", "Francesca Frijia", "Vincenzo Positano", "Luca Menichetti", "Maria Filomena Santarelli", "Jan Henrik Ardenkjaer-Larsen", "Florian Wiesinger", "Vincenzo Lionetti", "Simone Lorenzo Romano", "Giacomo Bianchi", "Danilo Neglia", "Giulio Giovannetti", "Rolf F Schulte", "Fabio Anastasio Recchia", "Luigi Landini", "Massimo Lombardi"]
publication_types = ["2"]
abstract = ""
selected = "false"
publication = "*JACC Cardiovasc Imaging*"
doi = "10.1016/j.jcmg.2012.11.023"
+++

