+++
title = "[Easy and reproducible technique to address tricuspid valve regurgitation with patch augmentation"
date = 2013-11-01
authors = ["Marco Solinas", "Giacomo Bianchi", "Mattia Glauber"]
publication_types = ["2"]
abstract = "We describe a patch augmentation of the anterior leaflet of the tricuspid valve to address tricuspid valve regurgitation due to leaflet retraction. The area of the anterior leaflet is measured using a tricuspid valve annuloplasty sizer; a glutaraldehyde-fixed autologous pericardial patch is trimmed with slight 5 mm oversize. The anterior leaflet is detached and the patch is sutured with three 5/0 running interlocked sutures, then an annuloplasty is performed with an undersized ring. This technique offers an easy and reproducible tool to address tricuspid regurgitation due to lack of leaflet coaptation."
selected = "true"
publication = "*J Card Surg*"
doi = "10.1111/jocs.12151"
+++

