+++
title = "Aortic arch reconstruction in newborns with an autologous pericardial patch: contemporary results"
date = 2013-03-01
authors = ["Massimo Bernabei", "Rafik Margaryan", "Luigi Arcieri", "Giacomo Bianchi", "Vitali Pak", "Bruno Murzi"]
publication_types = ["2"]
abstract = "OBJECTIVES: The incidence of recurrent aortic arch obstruction after Norwood procedure and other types of aortic arch reconstruction in newborns remains high. Biological and synthetic materials are used to enlarge the aorta. We report our experience using autologous pericardium to reconstruct the aortic arch in patients with hypoplastic left heart syndrome, aortic arch interruption and hypoplastic aortic arch.METHODS: A retrospective analysis of 39 consecutively operated patients evaluated after an initial Norwood and other types aortic arch repair was performed. The presence of recurrent arch obstruction (mean gradient ≥ 20 mmHg) and its management were noted. The mean weight of our patients was 3.2 $pm$ 0.7 kg.RESULTS: The mean age at primary surgical correction was 7.4 $pm$ 6.8 (range 1-35 days). All patients were discharged without a significant residual gradient at the aortic arch except 4 who had a peak gradient of ≥ 30 mmHg. The overall incidence of recurrent arch obstruction was 28.2% (11 patients). Four (12.1%) patients had a distal obstruction, 1 (3%) had proximal obstruction and 1 had a mid-transverse arch obstruction. All patients underwent aortic arch reintervention consisting of balloon dilatation, and only after unsuccessful dilatation, 3 underwent surgical patch aortoplasties.CONCLUSIONS: The use of autologous pericardium in aortic arch reconstruction procedure is effective and associated with an acceptable incidence of recurrent arch obstruction. Its availability and characteristics make it an attractive alternative to other materials."
selected = "false"
publication = "*Interact Cardiovasc Thorac Surg*"
doi = "10.1093/icvts/ivs510"
+++

