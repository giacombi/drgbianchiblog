+++
title = "Pulmonary artery perforation by plug anchoring system after percutaneous closure of left appendage"
date = 2013-07-01
authors = ["Giacomo Bianchi", "Marco Solinas", "Tommaso Gasbarri", "Stefano Bevilacqua", "Kaushal Kishore Tiwari", "Sergio Berti", "Mattia Glauber"]
publication_types = ["2"]
abstract = "Patients receiving oral anticoagulant therapy for atrial fibrillation who are at high risk of bleeding are increasingly referred for percutaneous left atrial appendage exclusion. Although effective, this procedure is not free from risk. We report a case of pericardial tamponade due to pulmonary artery tear caused by a trespassing anchoring hook of an AGA plug. Intraoperatively, no actual bleeding was found from the left appendage, a proof of its complete occlusion by the device. The patient underwent successful surgical repair and radio-frequency ablation of atrial fibrillation was performed by pulmonary veins encircling. "
selected = "false"
publication = "*Ann Thorac Surg*"
doi = "10.1016/j.athoracsur.2012.12.057"
+++

