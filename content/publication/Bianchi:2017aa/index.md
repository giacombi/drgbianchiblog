+++
title = "Outcomes of Video-assisted Minimally Invasive Cardiac Myxoma Resection"
date = 2017-12-01
authors = ["Giacomo Bianchi", "Rafik Margaryan", "Enkel Kallushi", "Alfredo Giuseppe Cerillo", "Pier Andrea Farneti", "Angela Pucci", "Marco Solinas"]
publication_types = ["2"]
abstract = "BACKGROUND: Myxomas are the most frequent cardiac tumours. Their diagnosis requires prompt removal. In our centre, for valve surgery we use a minimally invasive approach. Here, we report our experience of cardiac myxoma removal through right lateral mini-thoracotomy (RLMT) with particular focus on its feasibility, efficacy and patient safety.METHODS: Between February 2006 and January 2017, 30 consecutive patients (aged 66$pm$12.6years, range 35-83 years) underwent atrial myxoma resection through video-assisted RLMT. Percutaneous venous drainage was performed in all patients and direct cannulation of the ascending aorta was performed in 28 out of 30 (93.3%). The diagnosis of atrial myxoma was confirmed by histology.RESULTS: Complete surgical resection was achieved in all patients. The mean cardiopulmonary bypass (CPB) time was 76.5$pm$40.8minutes and average aortic cross-clamping time was 41.5$pm$29.8minutes. No patient suffered postoperative complications. Five patients (16.7%) received a blood transfusion. Mechanical ventilation ranged from 3 to 51hours (median 6hours), intensive care unit (ICU) stay ranged from 1 to 5days (median 1day). Total hospital length of stay (HLOS) was 5.6$pm$2 days. Home discharge rate was 56.7%. No in-hospital mortality was reported. During follow-up (55.6$pm$32.3 months; range 4-132 months), one tumour recurrence was observed. There were three late non-cardiac deaths. Overall survival was 100%, 85.7% and 85.7% at 1, 5 and 10 years, respectively.CONCLUSIONS: The use of video-assisted RLMT is an effective and reproducible strategy in all patients requiring expedited surgery for left atrial myxoma, independently of coexisting morbidity such as systemic embolisation or previous surgery. This technique leads to complete tumour resection, prompt recovery, early home discharge and high freedom from both symptoms and tumour recurrence."
selected = "true"
publication = "*Heart Lung Circ*"
tags = ["Heart valves; Minimally invasive surgery; Myxoma; Outcomes"]
doi = "10.1016/j.hlc.2017.11.010"
+++

