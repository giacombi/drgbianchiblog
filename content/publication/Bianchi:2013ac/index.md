+++
title = "Early bioprosthetic mitral valve degeneration due to subchordal apparatus impingement"
date = 2013-03-01
authors = ["Giacomo Bianchi", "Marco Solinas", "Daniyar Gilmanov", "Mattia Glauber"]
publication_types = ["2"]
abstract = "We present a case of early degeneration of a bioprosthesis in the mitral position three years after implantation. Valve explantation revealed complete neo-intima formation and complete fusion of one commissure due to papillary muscle and chordae tendineae embedding in the bioprosthetic leaflets."
selected = "false"
publication = "*J Card Surg*"
doi = "10.1111/jocs.12057"
+++

