+++
title = "Control of autocrine and paracrine myocardial signals: an emerging therapeutic strategy in heart failure"
date = 2010-11-01
authors = ["Vincenzo Lionetti", "Giacomo Bianchi", "Fabio A Recchia", "Carlo Ventura"]
publication_types = ["2"]
abstract = "A growing body of evidence supports the hypothesis that autocrine and paracrine mechanisms, mediated by factors released by the resident cardiac cells, could play an essential role in the reparative process of the failing heart. Such signals may influence the function of cardiac stem cells via several mechanisms, among which the most extensively studied are cardiomyocyte survival and angiogenesis. Moreover, besides promoting cytoprotection and angiogenesis, paracrine factors released by resident cardiac cells may alter cardiac metabolism and extracellular matrix turnover, resulting in more favorable post-injury remodeling. It is reasonable to believe that critical intracellular signals are activated and modulated in a temporal and spatial manner exerting different effects, overall depending on the microenvironment changes present in the failing myocardium. The recent demonstration that chemically, mechanically or genetically activated cardiac cells may release peptides to protect tissue against ischemic injury provides a potential route to achieve the delivery of specific proteins produced by these cells for innovative pharmacological regenerative therapy of the heart. It is important to keep in mind that therapies currently used to treat heart failure (HF) and leading to improvement of cardiac function fail to induce tissue repair/regeneration. As a matter of facts, if specific autocrine/paracrine cell-derived factors that improve cardiac function will be identified, pharmacological-based therapy might be more easily translated into clinical benefits than cell-based therapy. This review will focus on the recent development of potential pharmacologic targets to promote and drive at molecular level the cardiac repair/regeneration in HF."
selected = "false"
publication = "*Heart Fail Rev*"
doi = "10.1007/s10741-010-9165-7"
+++

