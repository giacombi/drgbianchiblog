+++
title = "In vivo experience of the child-size pediatric Jarvik 2000 heart: update"
date = 2010-01-01
authors = ["Marc Gibber", "Zhongjun J Wu", "Won-Bae Chang", "Giacomo Bianchi", "Jingping Hu", "Jose Garcia", "Robert Jarvik", "Bartley P Griffith"]
publication_types = ["2"]
abstract = "Data from early in vivo experiments demonstrated that the child-size Jarvik heart was capable of providing partial to nearly complete circulatory support with acceptable adverse effects on blood. However, bearing thrombosis was responsible for device malfunction in most cases. To overcome this problem, original pin bearings were replaced with novel conical bearings. This study evaluated chronic in vivo performance of the modified child-size Jarvik heart in the pediatric setting. Six juvenile sheep were implanted with the modified child-size Jarvik heart. Cardiac and pump output were measured daily. Serial blood samples were drawn to evaluate hematology, biocompatibility, and end-organ function. End-organ damage and device thrombosis were examined at necropsy. No device malfunction occurred during animal experiments up to 70 days. Mean cardiac output of the animals was 3.4 L/min. The child-size Jarvik heart was able to deliver a blood flow ranging from 1.4 to 2.5 L/min at speed from 10,000 rpm to 14,000 rpm. Mean plasma-free hemoglobin was 9.8 +/- 5.6 mg/dl, indicating no hemolysis. Acute elevation occurred in some organ function tests after the implant surgery but returned to normal range thereafter. These indices and necropsy showed no end-organ damage. No device thrombosis was observed. The current in vivo experience shows that the modified child Jarvik 2000 heart retained its hemodynamic function and excellent biocompatibility, and the conical bearings permitted it to remain free of thrombus."
selected = "false"
publication = "*ASAIO J*"
doi = "10.1097/MAT.0b013e3181dbe55e"
+++

