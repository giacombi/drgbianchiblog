+++
title = "Giant solitary fibrous tumor of the epicardium causing reversible heart failure"
date = 2013-08-01
authors = ["Giacomo Bianchi", "Matteo Ferrarini", "Marco Matteucci", "Angelo Monteleone", "Giovanni Donato Aquaro", "Claudio Passino", "Angela Pucci", "Mattia Glauber"]
publication_types = ["2"]
abstract = "A 68-year-old woman with a 2-year history of dyspnea and fatigue was admitted to our hospital with a massive pericardial effusion. Computed tomography and cardiovascular magnetic resonance imaging revealed a huge (17 cm maximum diameter) intrapericardial mass. After successful tumor resection, a giant solitary fibrous tumour of the epicardium was diagnosed by histology. Histologic features of malignancy were absent, and the patient is alive and well 1 year after the operation, undergoing close follow-up at regular intervals. Recurrences have been exceptionally reported in benign solitary fibrous tumors, and experience with this exceptionally rare and enigmatic cardiac tumor is lacking."
selected = "false"
publication = "*Ann Thorac Surg*"
tags = ["18"]
doi = "10.1016/j.athoracsur.2013.02.053"
+++

