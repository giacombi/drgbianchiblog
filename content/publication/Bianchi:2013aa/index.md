+++
title = "Mechanical properties and biological interaction of aortic clamps: are these all minimally invasive?"
date = 2013-01-01
authors = ["Giacomo Bianchi", "Angela Pucci", "Marco Matteucci", "Egidio Varone", "Simone Lorenzo Romano", "Vincenzo Lionetti", "Mattia Glauber"]
publication_types = ["2"]
abstract = "OBJECTIVE: Although specifically designed aortic clamps are mainstay of minimally invasive cardiac surgery, so far, no comparative reports about their mechanical properties and interaction with the aortic wall have been reported. In this study, the generated force in the clamps' jaws and the biological response of the aorta after clamping are evaluated.METHODS: The jaw force of five commercially available clamps [Geister, Cygnet, Cardiovision (CV) 195.10, CV 195.40, and CV 195.83] was assessed by clamping a 2.2-mm compression load cell with a dedicated computer universal serial bus interface at the proximal, the middle, and the distal site from the fulcrum. Biological response of the aortic wall was assessed in five minipigs (weight, 38-40 kg) that underwent thoracic aorta clamping and leakage point test. Immunohistochemistry and morphometric analysis were carried out for each aortic segment tested.RESULTS: Force generation pattern is peculiar of each clamp, being higher in the proximal and the middle portion and lower in the distal part. One clamp (Cygnet) exhibited homogeneous maximal force generation at all three sites. All clamps exhibited peculiar crushing artifacts. A variable degree of endothelial layer disruption occurred in all clamping tests; three clamps (CV 195.10, Cygnet, and Geister) had the lower amount of intact endothelium. The clamping force was not associated with the degree of endothelial disruption (P value was not significant).CONCLUSIONS: The choice of a clamp that is not only minimally invasive in design but also least traumatic will help avoid complications of aortic manipulation."
selected = "false"
publication = "*Innovations (Phila)*"
doi = "10.1097/IMI.0b013e31828d4903"
+++

