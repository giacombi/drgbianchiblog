+++
title = "Minimally invasive mitral valve surgery in high-risk patients: operating outside the boxplot"
date = 2016-06-01
authors = ["Marco Moscarelli", "Alfredo Cerillo", "Thanos Athanasiou", "Pierandrea Farneti", "Giacomo Bianchi", "Rafik Margaryan", "Marco Solinas"]
publication_types = ["2"]
abstract = "OBJECTIVES: (i) To establish who is at high risk for mitral surgery. (ii) To assess the performance of minimally invasive mitral valve surgery in high-risk patients by presenting early and late outcomes and compare these with those of the non-high-risk population.METHODS: We reviewed our database of prospective data of 1873 consecutive patients who underwent minimally invasive mitral surgery from 2003 to 2015. To establish an unbiased definition of risk cut-off, we considered as high-risk the 'outliers of risk' identified using boxplot analysis in relation to EuroSCORE II.RESULTS: Two hundred and five patients were outliers, with 98 as minor (EuroSCORE II ≥ 6%) and 107 as major outliers (EuroSCORE II ≥ 9%). Outliers accounted for several different comorbidities. Nineteen patients died while in hospital (9.2%); different postoperative complications were observed. Outliers had a significantly lower mean survival time and a higher risk of cardiac-related death than the general population; however, the worst outcomes were observed in major outliers. No statistically significant difference was found with regard to the need for mitral reintervention and the degree of mitral regurgitation at follow-up.CONCLUSIONS: Boxplot analysis helped to achieve an internal definition of risk cut-off, starting from EuroSCORE II ≥ 6%. Minimally invasive mitral surgery in these outliers of risk was associated with acceptable early and long-term results; however, major outliers with EuroSCORE II ≥ 9% may benefit from catheter-based procedures."
selected = "false"
publication = "*Interact Cardiovasc Thorac Surg*"
tags = ["Minimally invasive surgery; Mitral valve; Outcomes; Risk analysis/modelling; Statistics"]
doi = "10.1093/icvts/ivw038"
+++

