+++
title = "eComment. The use of mechanical assistance devices in post-infarction ventricular septal defects"
date = 2013-02-01
authors = ["Giacomo Bianchi", "Marco Solinas", "Mattia Glauber"]
publication_types = ["2"]
abstract = ""
selected = "false"
publication = "*Interact Cardiovasc Thorac Surg*"
doi = "10.1093/icvts/ivs569"
+++

