+++
title = "Surgical resection of colorectal carcinomas metastatic to the heart"
date = 2016-11-01
authors = ["Giacomo Bianchi", "Alfredo Giuseppe Cerillo", "Michele Murzi", "Marco Solinas"]
publication_types = ["2"]
abstract = "Colorectal carcinomas metastatic to the heart are rare. We present a case of colonic carcinoma metastatic to the right atrium and discuss the management and prognosis of these rare lesions."
selected = "true"
publication = "*J Card Surg*"
doi = "10.1111/jocs.12841"
+++

